<?php

namespace Database\Seeders;

use App\DAL\ShopDAL;
use Illuminate\Database\Seeder;

class ShopSeeder extends Seeder
{
    private ShopDAL $shopDAL;

    public function __construct ( ShopDAL $shopDAL )
    {
        $this->shopDAL = $shopDAL;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $this->shopDAL->create( [
            'name'    => 'shop name',
            'user_id' => 2,
            'lat'     => 50.12,
            'long'    => 103.006
        ] );
    }
}
