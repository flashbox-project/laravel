<?php

namespace Database\Seeders;

use App\DAL\CategoryDAL;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    protected CategoryDAL $categoryDAL;

    public function __construct ( CategoryDAL $categoryDAL )
    {
        $this->categoryDAL = $categoryDAL;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $this->categoryDAL->create( [
            'name' => 'technology',
        ] );

        $this->categoryDAL->create( [
            'name' => 'beauty',
        ] );

    }
}
