<?php

namespace Database\Seeders;

use App\DAL\OrderDAL;
use App\DAL\OrderMetaDAL;
use App\Models\Order;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    private OrderDAL $orderDAL;
    private OrderMetaDAL $orderMetaDAL;

    public function __construct ( OrderDAL $orderDAL, OrderMetaDAL $orderMetaDAL )
    {
        $this->orderDAL     = $orderDAL;
        $this->orderMetaDAL = $orderMetaDAL;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        /**
         * @var Order $order
         */
        $order = $this->orderDAL->create( [
            'user_id' => 2,
            'status'  => Order::STATUS_OPEN,
            'lat'     => 123.23,
            'long'    => 59.2354,
        ] );

        $this->orderMetaDAL->create( [
            'order_id'   => $order->id,
            'product_id' => 1,
        ] );

        $this->orderMetaDAL->create( [
            'order_id'   => $order->id,
            'product_id' => 2,
        ] );
    }
}
