<?php

namespace Database\Seeders;

use App\DAL\PermissionDAL;
use App\DAL\RoleDAL;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Spatie\Permission\PermissionRegistrar;

class RolesAndPermissionsSeeder extends Seeder
{
    private RoleDAL $roleDAL;
    private PermissionDAL $permissionDAL;

    public function __construct ( RoleDAL $roleDAL, PermissionDAL $permissionDAL )
    {
        $this->roleDAL       = $roleDAL;
        $this->permissionDAL = $permissionDAL;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        app()[ PermissionRegistrar::class ]->forgetCachedPermissions();

        // Create Permissions
        foreach ( Permission::PERMISSIONS as $key => $permission_name )
        {
            $this->permissionDAL->create( [
                'name'       => $permission_name,
                'guard_name' => 'api',
            ] );
        }

        /**
         * @var Role $admin_role
         */
        $admin_role = $this->roleDAL->create( [
            'name'       => Role::ADMIN_ROLE_NAME,
            'guard_name' => 'api',
        ] );
        // Category
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'list_category' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'create_category' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'show_category' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'update_category' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'delete_category' ] );
        // Shop
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'list_shop' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'create_shop' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'show_shop' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'update_shop' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'delete_shop' ] );
        // Product
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'list_product' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'create_product' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'show_product' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'update_product' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'delete_product' ] );
        // Order
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'list_order' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'create_order' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'show_order' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'update_order' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'delete_order' ] );
        // Order Meta
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'create_order_meta' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'delete_order_meta' ] );
        // Transactions
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'list_transaction' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'show_transaction' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'update_transaction' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'delete_transaction' ] );
        // Seller Transactions
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'list_seller_transaction' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'show_seller_transaction' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'update_seller_transaction' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'delete_seller_transaction' ] );
        // Users
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'create_seller' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'list_user' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'show_user' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'update_user' ] );
        $this->roleDAL->givePermissionToRole( $admin_role, Permission::PERMISSIONS[ 'delete_user' ] );


        /**
         * @var Role $seller_role
         */
        $seller_role = $this->roleDAL->create( [
            'name'       => Role::SELLER_ROLE_NAME,
            'guard_name' => 'api',
        ] );
        // Category
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'list_category' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'show_category' ] );
        // Shop
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'list_shop' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'create_shop' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'show_shop' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'update_shop' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'delete_shop' ] );
        // Product
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'list_product' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'create_product' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'show_product' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'update_product' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'delete_product' ] );
        // Order
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'list_order' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'create_order' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'show_order' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'update_order' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'delete_order' ] );
        // Order Meta
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'create_order_meta' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'delete_order_meta' ] );
        // Transactions
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'list_transaction' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'show_transaction' ] );
        // Seller Transactions
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'list_seller_transaction' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'show_seller_transaction' ] );
        // Users
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'show_user' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'update_user' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'delete_user' ] );

        /**
         * @var Role $customer_role
         */
        $customer_role = $this->roleDAL->create( [
            'name'       => Role::CUSTOMER_ROLE_NAME,
            'guard_name' => 'api',
        ] );
        // Category
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'list_category' ] );
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'show_category' ] );
        // Shop
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'list_shop' ] );
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'show_shop' ] );
        // Product
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'list_product' ] );
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'show_product' ] );
        // Order
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'list_order' ] );
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'create_order' ] );
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'show_order' ] );
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'update_order' ] );
        // Order Meta
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'create_order_meta' ] );
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'delete_order_meta' ] );
        // Transactions
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'list_transaction' ] );
        $this->roleDAL->givePermissionToRole( $customer_role, Permission::PERMISSIONS[ 'show_transaction' ] );
        // Users
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'show_user' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'update_user' ] );
        $this->roleDAL->givePermissionToRole( $seller_role, Permission::PERMISSIONS[ 'delete_user' ] );


    }
}
