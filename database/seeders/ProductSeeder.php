<?php

namespace Database\Seeders;

use App\DAL\ProductDAL;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    private ProductDAL $productDAL;

    public function __construct ( ProductDAL $productDAL )
    {
        $this->productDAL = $productDAL;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $this->productDAL->create( [
            'name'        => 'name of product',
            'category_id' => 1,
            'shop_id'     => 1,
            'price'       => 1250000,
        ] );

        $this->productDAL->create( [
            'name'        => 'name of another product',
            'category_id' => 1,
            'shop_id'     => 1,
            'price'       => 5250000,
        ] );
    }
}
