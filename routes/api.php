<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderMetaController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SellerTransactionController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\TransactionController;
use App\Models\Permission;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

Route::group( [
    'prefix' => 'v1',
], function () {

    // Auth
    Route::group( [
        'prefix' => 'auth',
    ], function () {

        Route::group( [
            'middleware' => 'guest',
        ], function () {

            Route::post( '/register', [
                AuthController::class,
                'register'
            ] );

            Route::post( '/login', [
                AuthController::class,
                'login'
            ] );

        } );

        Route::group( [
            'middleware' => 'auth'
        ], function () {

            Route::get( '/logout', [
                AuthController::class,
                'logout'
            ] );

        } );

    } );

    // Users
    Route::group( [
        'prefix'     => 'user',
        'middleware' => 'auth',
    ], function () {

        Route::post( '/', [
            UserController::class,
            'storeSeller'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'create_seller' ] );

        Route::get( '/', [
            UserController::class,
            'list'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'list_user' ] );

        Route::group( [
            'prefix' => '{id}',
        ], function () {

            Route::get( '/', [
                UserController::class,
                'show'
            ])->middleware( 'permission:' . Permission::PERMISSIONS[ 'show_user' ] );

            Route::put( '/', [
                UserController::class,
                'update'
            ])->middleware( 'permission:' . Permission::PERMISSIONS[ 'update_user' ] );

            Route::delete( '/', [
                UserController::class,
                'delete'
            ])->middleware( 'permission:' . Permission::PERMISSIONS[ 'delete_user' ] );

        } )->whereNumber( 'id' );

    } );

    // Category
    Route::group( [
        'prefix'     => 'categories',
        'middleware' => 'auth',
    ], function () {

        Route::get( '/', [
            CategoryController::class,
            'list'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'list_category' ] );

        Route::post( '/', [
            CategoryController::class,
            'store'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'create_category' ] );

        Route::group( [
            'prefix' => '{id}',
        ], function () {

            Route::get( '/', [
                CategoryController::class,
                'show'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'show_category' ] );

            Route::delete( '/', [
                CategoryController::class,
                'delete'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'delete_category' ] );

            Route::put( '/', [
                CategoryController::class,
                'update'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'update_category' ] );

        } )->whereNumber( 'id' );

    } );

    // Shops
    Route::group( [
        'prefix'     => 'shops',
        'middleware' => 'auth',
    ], function () {


        Route::get( '/', [
            ShopController::class,
            'list'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'create_shop' ] );

        Route::post( '/', [
            ShopController::class,
            'store'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'create_shop' ] );

        Route::group( [
            'prefix' => '{id}'
        ], function () {

            Route::get( '/', [
                ShopController::class,
                'show'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'show_shop' ] );

            Route::put( '/', [
                ShopController::class,
                'update'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'update_shop' ] );

            Route::delete( '/', [
                ShopController::class,
                'delete'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'delete_shop' ] );

        } )->whereNumber( 'id' );

    } );

    // Products
    Route::group( [
        'prefix'     => 'products',
        'middleware' => 'auth',
    ], function () {

        Route::get( '/', [
            ProductController::class,
            'list'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'list_product' ] );

        Route::post( '/', [
            ProductController::class,
            'store'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'create_product' ] );

        Route::group( [
            'prefix' => '{id}'
        ], function () {

            Route::get( '/', [
                ProductController::class,
                'show'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'show_product' ] );

            Route::put( '/', [
                ProductController::class,
                'update'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'update_product' ] );

            Route::delete( '/', [
                ProductController::class,
                'delete'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'delete_product' ] );

        } )->whereNumber( 'id' );

    } );

    // Orders
    Route::group( [
        'prefix'     => 'orders',
        'middleware' => 'auth',
    ], function () {

        Route::get( '/', [
            OrderController::class,
            'list'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'list_order' ] );

        Route::post( '/', [
            OrderController::class,
            'store'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'create_order' ] );

        Route::group( [
            'prefix' => '{id}'
        ], function () {

            Route::get( '/', [
                OrderController::class,
                'show'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'show_order' ] );

            Route::put( '/', [
                OrderController::class,
                'update'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'update_order' ] );

            Route::delete( '/', [
                OrderController::class,
                'delete'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'delete_order' ] );

            Route::post( '/', [
                OrderMetaController::class,
                'store'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'create_order_meta' ] );


            Route::group( [
                'prefix' => '{meta_id}',
            ], function () {

                Route::delete( '/', [
                    OrderMetaController::class,
                    'delete'
                ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'delete_order_meta' ] );

            } )->whereNumber( 'meta_id' );

        } )->whereNumber( 'id' );

    } );

    // Transactions
    Route::group( [
        'prefix'     => 'transactions',
        'middleware' => 'auth',
    ], function () {

        Route::get( '/', [
            TransactionController::class,
            'list'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'list_transaction' ] );

        Route::post( '/callback', [
            TransactionController::class,
            'store'
        ] )->middleware( 'transaction.callback.token' );

        Route::group( [
            'prefix' => '{id}',
        ], function () {

            Route::get( '/', [
                TransactionController::class,
                'show'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'show_transaction' ] );

            Route::put( '/', [
                TransactionController::class,
                'update'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'update_transaction' ] );

            Route::delete( '/', [
                TransactionController::class,
                'delete'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'delete_transaction' ] );

        } )->whereNumber( 'id' );

    } );

    // Seller Transactions
    Route::group( [
        'prefix'     => 'seller-transactions',
        'middleware' => 'auth',
    ], function () {

        Route::get( '/', [
            SellerTransactionController::class,
            'list'
        ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'list_seller_transaction' ] );

        Route::group( [
            'prefix' => '{id}',
        ], function () {

            Route::get( '/', [
                SellerTransactionController::class,
                'show'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'show_seller_transaction' ] );

            Route::put( '/', [
                SellerTransactionController::class,
                'update'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'update_seller_transaction' ] );

            Route::delete( '/', [
                SellerTransactionController::class,
                'delete'
            ] )->middleware( 'permission:' . Permission::PERMISSIONS[ 'delete_seller_transaction' ] );

        } )->whereNumber( 'id' );

    } );

} );
