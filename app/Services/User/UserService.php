<?php

namespace App\Services\User;

use App\DAL\RoleDAL;
use App\DAL\UserDAL;
use App\Models\Role;
use App\Models\User;
use Nette\Schema\ValidationException;

class UserService implements UserServiceInterface
{
    private UserDAL $userDAL;

    public function __construct ( UserDAL $userDAL )
    {
        $this->userDAL = $userDAL;
    }

    public function list (): array
    {
        return $this->userDAL->all();
    }

    public function createSeller ( User $user ): User
    {
        $this->userDAL->beginTransaction();

        try
        {
            $roleDAL = new RoleDAL();
            /**
             * @var User $user
             */
            $user = $this->userDAL->save( $user );

            $roleDAL->assignRoleToUser( $user, Role::SELLER_ROLE_NAME );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->userDAL->rollback();

            throw $exception;
        }

        $this->userDAL->commit();

        return $user;
    }

    public function find ( int $id ): User
    {
        /**
         * @var User $user
         */
        $user = $this->userDAL->fetchByID( $id );

        return $user;
    }

    public function update ( User $user ): User
    {
        $this->userDAL->beginTransaction();

        try
        {
            /**
             * @var User $user
             */
            $user = $this->userDAL->update( $user );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->userDAL->rollback();

            throw $exception;
        }

        $this->userDAL->commit();

        return $user;
    }

    public function delete ( int $id ): bool
    {
        $this->userDAL->beginTransaction();

        try
        {
            $deleted = $this->userDAL->delete( $id );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->userDAL->rollback();

            throw $exception;
        }

        $this->userDAL->commit();

        return $deleted;
    }

}
