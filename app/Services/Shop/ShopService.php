<?php

namespace App\Services\Shop;

use App\DAL\ShopDAL;
use App\Models\Shop;
use Nette\Schema\ValidationException;

class ShopService implements ShopServiceInterface
{
    private ShopDAL $shopDAL;

    public function __construct ( ShopDAL $shopDAL )
    {
        $this->shopDAL = $shopDAL;
    }

    public function create ( Shop $shop ): Shop
    {
        $this->shopDAL->beginTransaction();

        try
        {
            /**
             * @var Shop $shop
             */
            $shop = $this->shopDAL->save( $shop );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->shopDAL->rollback();

            throw $exception;
        }

        $this->shopDAL->commit();

        return $shop;
    }

    public function find ( int $id ): Shop
    {
        /**
         * @var Shop $shop
         */
        $shop = $this->shopDAL->fetchByID( $id );

        return $shop;
    }

    public function update ( Shop $shop ): Shop
    {

        try
        {
            /**
             * @var Shop $updated_shop
             */
            $updated_shop = $this->shopDAL->update( $shop );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->shopDAL->rollback();

            throw $exception;
        }

        $this->shopDAL->commit();

        return $updated_shop;
    }

    public function delete ( int $id ): bool
    {
        try
        {
            $deleted = $this->shopDAL->delete( $id );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->shopDAL->rollback();

            throw $exception;
        }

        $this->shopDAL->commit();

        return $deleted;
    }

    public function list (float $lat, float $long, float $radius)
    {
        return $this->shopDAL->nearbyList($lat, $long, $radius);
    }
}
