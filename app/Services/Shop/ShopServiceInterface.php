<?php

namespace App\Services\Shop;

use App\Models\Shop;

interface ShopServiceInterface
{
    public function create ( Shop $shop ): Shop;

    public function find ( int $id ): Shop;

    public function update ( Shop $shop ): Shop;

    public function delete ( int $id ): bool;

    public function list ( float $lat, float $long, float $radius );
}
