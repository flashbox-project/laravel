<?php

namespace App\Services\Transaction;

use App\DAL\SellerTransactionDAL;
use App\DAL\TransactionDAL;
use App\Models\Role;
use App\Models\Transaction;
use App\Models\User;
use App\Services\SellerTransaction\SellerTransactionService;
use Illuminate\Support\Facades\Auth;
use Nette\Schema\ValidationException;

class TransactionService implements TransactionServiceInterface
{
    private TransactionDAL $transactionDAL;

    public function __construct ( TransactionDAL $transactionDAL )
    {
        $this->transactionDAL = $transactionDAL;
    }

    public function store ( Transaction $transaction ): void
    {
        $this->transactionDAL->beginTransaction();

        try
        {
            $transaction->status = Transaction::STATUS_SUCCESS;

            /**
             * @var Transaction $transaction
             */
            $transaction = $this->transactionDAL->save( $transaction );

            $seller_transaction_service = new SellerTransactionService( ( new SellerTransactionDAL() ) );

            $seller_transaction_service->storeFromTransaction($transaction);
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->transactionDAL->rollback();

            throw $exception;
        }

        $this->transactionDAL->commit();
    }

    public function list ( User $user ): array
    {
        if ( $user->hasRole( Role::ADMIN_ROLE_NAME ) )
        {
            $transactions = $this->transactionDAL->all();
        }
        else
        {
            $transactions = $this->transactionDAL->allByUserID( Auth::user()->id );
        }

        return $transactions;
    }

    public function find ( int $id ): Transaction
    {
        /**
         * @var Transaction
         */
        $transaction = $this->transactionDAL->fetchByID( $id );

        return $transaction;
    }

    public function update ( Transaction $transaction ): Transaction
    {
        $this->transactionDAL->beginTransaction();

        try
        {
            /**
             * @var Transaction $transaction
             */
            $transaction = $this->transactionDAL->save( $transaction );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->transactionDAL->rollback();

            throw $exception;
        }

        $this->transactionDAL->commit();

        return $transaction;
    }

    public function delete ( int $id ): bool
    {
        $this->transactionDAL->beginTransaction();

        try
        {
            $deleted = $this->transactionDAL->delete( $id );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->transactionDAL->rollback();

            throw $exception;
        }

        $this->transactionDAL->commit();

        return $deleted;
    }
}
