<?php

namespace App\Services\Transaction;

use App\Models\Transaction;
use App\Models\User;

interface TransactionServiceInterface
{
    public function store ( Transaction $transaction ): void;

    public function list ( User $user ): array;

    public function find ( int $id ): Transaction;

    public function update ( Transaction $transaction ): Transaction;

    public function delete ( int $id ): bool;
}
