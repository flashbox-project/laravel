<?php

namespace App\Services\SellerTransaction;

use App\Models\SellerTransaction;
use App\Models\Transaction;
use App\Models\User;

interface SellerTransactionServiceInterface
{
    public function storeFromTransaction ( Transaction $transaction ): void;

    public function list ( User $user ): array;

    public function find ( int $id ): SellerTransaction;

    public function update ( SellerTransaction $seller_transaction ): SellerTransaction;

    public function delete ( int $id ): bool;
}
