<?php

namespace App\Services\SellerTransaction;

use App\DAL\OrderMetaDAL;
use App\DAL\SellerTransactionDAL;
use App\Models\Role;
use App\Models\SellerTransaction;
use App\Models\Transaction;
use App\Models\User;
use Nette\Schema\ValidationException;

class SellerTransactionService implements SellerTransactionServiceInterface
{
    private SellerTransactionDAL $sellerTransactionDAL;

    public function __construct ( SellerTransactionDAL $sellerTransactionDAL )
    {
        $this->sellerTransactionDAL = $sellerTransactionDAL;
    }

    public function storeFromTransaction ( Transaction $transaction ): void
    {
        $order_meta_dal = new OrderMetaDAL();
        $order_metas    = $order_meta_dal->fetchListByOrderID( $transaction->order_id );

        foreach ( $order_metas as $order_meta )
        {
            /**
             * @var SellerTransaction $seller_transaction
             */
            $seller_transaction = $this->sellerTransactionDAL->create( [
                'shop_id' => $order_meta->product->shop_id,
                'status'  => SellerTransaction::STATUS_OPEN,
                'price'   => $order_meta->product->price,
            ] );

            $seller_transaction->orderMetas()->attach( $order_meta );
        }

    }

    public function list ( User $user ): array
    {
        if ( $user->hasRole( Role::ADMIN_ROLE_NAME ) )
        {
            $seller_transactions = $this->sellerTransactionDAL->all();
        }
        else
        {
            $seller_transactions = $this->sellerTransactionDAL->allByUserID( $user->id );
        }

        return $seller_transactions;
    }

    public function find ( int $id ): SellerTransaction
    {
        /**
         * @var SellerTransaction $seller_transaction
         */
        $seller_transaction = $this->sellerTransactionDAL->fetchByID( $id );

        return $seller_transaction;
    }

    public function update ( SellerTransaction $seller_transaction ): SellerTransaction
    {
        $this->sellerTransactionDAL->beginTransaction();

        try
        {
            /**
             * @var SellerTransaction $seller_transaction
             */
            $seller_transaction = $this->sellerTransactionDAL->save( $seller_transaction );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->sellerTransactionDAL->rollback();

            throw $exception;
        }

        $this->sellerTransactionDAL->commit();

        return $seller_transaction;
    }

    public function delete ( int $id ): bool
    {
        $this->sellerTransactionDAL->beginTransaction();

        try
        {
            $deleted = $this->sellerTransactionDAL->delete( $id );
        }
        catch ( ValidationException|\Exception $exception )
        {
            $this->sellerTransactionDAL->rollback();

            throw $exception;
        }

        $this->sellerTransactionDAL->commit();

        return $deleted;
    }
}
