<?php

namespace App\Services\Auth;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;

interface AuthServiceInterface
{
    public function register ( User $user ): array;

    public function login ( string $email, string $hashed_password ): array;

    # Role And Permissions
    public function createRole ( Role $role ): Role;

    public function createPermission ( Permission $permission ): Permission;

    public function assignRoleToUser ( User $user, string ...$role ): void;

    public function assignPermissionToRole ( Role $role, string ...$permission ): void;

    public function logout ( User $user ): void;
}
