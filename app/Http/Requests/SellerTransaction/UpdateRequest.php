<?php

namespace App\Http\Requests\SellerTransaction;

use App\Models\SellerTransaction;

class UpdateRequest extends FetchRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules ()
    {
        return array_merge( parent::rules(), [
            'shop_id'     => 'required|integer|exists:shops,id',
            'refrence_id' => 'nullable|string|unique:transactions,refrence_id',
            'status'      => 'required|integer|in:' . implode( ',', [
                    SellerTransaction::STATUS_FAILED,
                    SellerTransaction::STATUS_SUCCESS,
                    SellerTransaction::STATUS_OPEN,
                ] ),
            'price'       => 'required|integer'
        ] );
    }
}
