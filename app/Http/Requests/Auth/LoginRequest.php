<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return true;
    }

    const EXISTS_EMAIL_MESSAGE = 'email or password is invalid';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules ()
    {
        return [
            'email'    => 'required|email|max:255|exists:users',
            'password' => 'required|string|min:8'
        ];
    }

    public function messages ()
    {
        return [
            'email.exists' => self::EXISTS_EMAIL_MESSAGE,
        ];
    }
}
