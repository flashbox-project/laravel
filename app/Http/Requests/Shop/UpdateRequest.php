<?php

namespace App\Http\Requests\Shop;

class UpdateRequest extends FetchRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules ()
    {
        return array_merge( parent::rules(), ( new StoreRequest() )->rules() );
    }
}
