<?php

namespace App\Http\Requests\User;

use App\Rules\User\UpdateUserEmailRule;

class UpdateRequest extends FetchRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules ()
    {
        return array_merge( parent::rules(), [
            'name'  => 'required|string|max:255',
            'email' => [
                'required',
                'email',
                'max:255',
                new UpdateUserEmailRule
            ],
        ] );
    }
}
