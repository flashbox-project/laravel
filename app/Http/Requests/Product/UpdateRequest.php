<?php

namespace App\Http\Requests\Product;

class UpdateRequest extends FetchRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules ()
    {
        return array_merge( parent::rules(), [
            'name'        => 'required|string|max:255',
            'category_id' => 'required|integer|exists:categories,id',
            'shop_id' => 'required|integer|exists:shops,id',
            'price'       => 'required|integer|min:1',
        ] );
    }
}
