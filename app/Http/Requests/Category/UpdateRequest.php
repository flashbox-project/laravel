<?php

namespace App\Http\Requests\Category;

class UpdateRequest extends FetchRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules ()
    {
        return array_merge( parent::rules(), [
            'name' => 'required|string|max:255',
        ] );
    }
}
