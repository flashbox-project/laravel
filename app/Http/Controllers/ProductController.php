<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\DeleteRequest;
use App\Http\Requests\Product\FetchRequest;
use App\Http\Requests\Product\StoreRequest;
use App\Http\Requests\Product\UpdateRequest;
use App\Http\Resources\Product\ProductResource;
use App\Models\Permission;
use App\Models\Product;
use App\Services\Product\ProductService;
use App\Services\Shop\ShopService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;

class ProductController extends Controller
{
    private ProductService $productService;
    private ShopService $shopService;

    public function __construct ( ProductService $productService, ShopService $shopService )
    {
        $this->productService = $productService;
        $this->shopService    = $shopService;
    }

    public function store ( StoreRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $shop = $this->shopService->find( $validated_data[ 'shop_id' ] );
        if ( $shop->user_id != Auth::user()->id )
        {
            throw PermissionDoesNotExist::create( Permission::PERMISSIONS[ 'create_product' ], 'api' );
        }

        $product              = new Product();
        $product->name        = $validated_data[ 'name' ];
        $product->category_id = $validated_data[ 'category_id' ];
        $product->shop_id     = $validated_data[ 'shop_id' ];
        $product->price       = $validated_data[ 'price' ];

        /**
         * @var Product $product
         */
        $product = $this->productService->create( $product );

        return response()->json( ProductResource::make( $product ), 201 );
    }

    public function show ( FetchRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $product = $this->productService->find( $validated_data[ 'id' ] );

        return response()->json( ProductResource::make( $product ) );
    }

    public function update ( UpdateRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $product = $this->productService->find( $validated_data[ 'id' ] );

        $shop = $this->shopService->find( $validated_data[ 'shop_id' ] );

        $product_shop = $product->shop;

        if ( $shop->user_id != Auth::user()->id || $product_shop->user_id != Auth::user()->id )
        {
            throw PermissionDoesNotExist::create( Permission::PERMISSIONS[ 'update_product' ], 'api' );
        }

        $product->name        = $validated_data[ 'name' ];
        $product->category_id = $validated_data[ 'category_id' ];
        $product->shop_id     = $validated_data[ 'shop_id' ];
        $product->price       = $validated_data[ 'price' ];

        /**
         * @var Product $product
         */
        $product = $this->productService->update( $product );

        return response()->json( ProductResource::make( $product ) );
    }

    public function delete ( DeleteRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $product = $this->productService->find( $validated_data[ 'id' ] );

        $product_shop = $product->shop;

        if ( $product_shop->user_id != Auth::user()->id )
        {
            throw PermissionDoesNotExist::create( Permission::PERMISSIONS[ 'delete_product' ], 'api' );
        }

        return response()->json( null, 204 );
    }

    public function list (): JsonResponse
    {
        $products = $this->productService->list();

        return response()->json( ProductResource::collection( $products ) );
    }
}
