<?php

namespace App\Http\Controllers;

use App\Http\Requests\Transaction\DeleteRequest;
use App\Http\Requests\Transaction\FetchRequest;
use App\Http\Requests\Transaction\StoreRequest;
use App\Http\Requests\Transaction\UpdateRequest;
use App\Http\Resources\Transaction\TransactionResource;
use App\Models\Transaction;
use App\Services\Order\OrderService;
use App\Services\Transaction\TransactionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    private TransactionService $transactionService;

    public function __construct ( TransactionService $transactionService, OrderService $orderService )
    {
        $this->transactionService = $transactionService;

        $this->orderService = $orderService;
    }

    public function list (): JsonResponse
    {
        $transactions = $this->transactionService->list( Auth::user() );

        return response()->json( TransactionResource::collection( $transactions ) );
    }

    public function store ( StoreRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $transaction              = new Transaction();
        $transaction->order_id    = $validated_data[ 'order_id' ];
        $transaction->price       = $validated_data[ 'price' ];
        $transaction->refrence_id = $validated_data[ 'refrence_id' ];

        $this->transactionService->store($transaction);

        return response()->json( null, 201 );
    }

    public function show ( FetchRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $transaction = $this->transactionService->find( $validated_data[ 'id' ] );

        return response()->json( TransactionResource::make( $transaction ) );
    }

    public function update ( UpdateRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $transaction = $this->transactionService->find( $validated_data[ 'id' ] );

        $transaction->refrence_id = $validated_data[ 'refrence_id' ];
        $transaction->status      = $validated_data[ 'status' ];

        $transaction = $this->transactionService->update( $transaction );

        return response()->json( TransactionResource::make( $transaction ) );
    }

    public function delete ( DeleteRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $this->transactionService->delete( $validated_data[ 'id' ] );

        return response()->json( null, 204 );
    }


}
