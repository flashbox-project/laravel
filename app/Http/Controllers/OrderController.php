<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\DeleteRequest;
use App\Http\Requests\Order\FetchRequest;
use App\Http\Requests\Order\StoreRequest;
use App\Http\Requests\Order\UpdateRequest;
use App\Http\Resources\Order\OrderMetaResource;
use App\Http\Resources\Order\OrderResource;
use App\Models\Order;
use App\Models\Permission;
use App\Services\Order\OrderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;

class OrderController extends Controller
{
    private OrderService $orderService;

    public function __construct ( OrderService $orderService )
    {
        $this->orderService = $orderService;
    }

    public function store ( StoreRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $order          = new Order();
        $order->user_id = Auth::user()->id;
        $order->lat     = $validated_data[ 'lat' ];
        $order->long    = $validated_data[ 'long' ];

        $result = $this->orderService->create( $order, $validated_data[ 'products' ] );

        return response()->json( [
            'order'       => OrderResource::make( $result[ 'order' ] ),
            'order_metas' => OrderMetaResource::collection( $result[ 'order_metas' ] ),
        ], 201 );
    }

    /**
     * @throws \Exception
     */
    public function show ( FetchRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $order = $this->orderService->find( $validated_data[ 'id' ] );

        if ( ! $this->orderService->checkUserOrderPermission( Auth::user(), $order, Permission::PERMISSIONS[ 'show_order' ] ) )
        {
            throw PermissionDoesNotExist::create( Permission::PERMISSIONS[ 'show_order' ], 'api' );
        }

        return response()->json( [
            'order'       => OrderResource::make( $order ),
            'order_metas' => OrderMetaResource::collection( $order->metas ),
        ], 201 );
    }

    public function delete ( DeleteRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $order = $this->orderService->find( $validated_data[ 'id' ] );

        if ( ! $this->orderService->checkUserOrderPermission( Auth::user(), $order, Permission::PERMISSIONS[ 'delete_order' ] ) )
        {
            throw PermissionDoesNotExist::create( Permission::PERMISSIONS[ 'delete_order' ], 'api' );
        }

        $this->orderService->delete( $order->id );

        return response()->json( null, 204 );
    }

    public function update ( UpdateRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $order = $this->orderService->find( $validated_data[ 'id' ] );

        if ( ! $this->orderService->checkUserOrderPermission( Auth::user(), $order, Permission::PERMISSIONS[ 'update_order' ] ) )
        {
            throw PermissionDoesNotExist::create( Permission::PERMISSIONS[ 'update_order' ], 'api' );
        }

        $order->lat    = $validated_data[ 'lat' ];
        $order->long   = $validated_data[ 'long' ];
        $order->status = $validated_data[ 'status' ];

        $order = $this->orderService->update( $order );

        return response()->json( [
            'order'       => OrderResource::make( $order ),
            'order_metas' => OrderMetaResource::collection( $order->metas ),
        ], 201 );
    }

    public function list(): JsonResponse
    {
        $orders = $this->orderService->list(Auth::user());

        return response()->json(OrderResource::collection($orders));
    }
}
