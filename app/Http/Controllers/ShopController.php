<?php

namespace App\Http\Controllers;

use App\Http\Requests\Shop\DeleteRequest;
use App\Http\Requests\Shop\FetchRequest;
use App\Http\Requests\Shop\ListRequest;
use App\Http\Requests\Shop\UpdateRequest;
use App\Http\Resources\Shop\ShopResource;
use App\Models\Shop;
use App\Services\Shop\ShopService;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\Shop\StoreRequest;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    private ShopService $shopService;

    public function __construct ( ShopService $shopService )
    {
        $this->shopService = $shopService;
    }

    public function list ( ListRequest $request ): JsonResponse
    {
        $validated_data = $request;


        $shops = $this->shopService->list( $validated_data[ 'lat' ], $validated_data[ 'long' ], $validated_data[ 'radius' ] );

        return response()->json( ShopResource::collection( $shops ) );
    }

    public function store ( StoreRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $shop          = new Shop();
        $shop->user_id = Auth::user()->id;
        $shop->name    = $validated_data[ 'name' ];
        $shop->lat     = $validated_data[ 'lat' ];
        $shop->long    = $validated_data[ 'long' ];

        $result = $this->shopService->create( $shop );

        return response()->json( ShopResource::make( $result ), 201 );
    }

    public function show ( FetchRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $result = $this->shopService->find( $validated_data[ 'id' ] );

        return response()->json( ShopResource::make( $result ) );
    }

    public function update ( UpdateRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $shop = $this->shopService->find( $validated_data[ 'id' ] );

        if ( $shop->user_id != Auth::user()->id )
        {
            return response()->json( [
                'message' => 'Permission denied'
            ], 403 );
        }

        $shop->name = $validated_data[ 'name' ];
        $shop->lat  = $validated_data[ 'lat' ];
        $shop->long = $validated_data[ 'long' ];

        $result = $this->shopService->update( $shop );

        return response()->json( ShopResource::make( $result ) );
    }

    public function delete ( DeleteRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $shop = $this->shopService->find( $validated_data[ 'id' ] );

        if ( $shop->user_id != Auth::user()->id )
        {
            return response()->json( [
                'message' => 'Permission denied'
            ], 403 );
        }

        $this->shopService->delete( $validated_data[ 'id' ] );

        return response()->json( null, 204 );
    }
}
