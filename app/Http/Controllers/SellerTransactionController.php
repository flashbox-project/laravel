<?php

namespace App\Http\Controllers;

use App\Http\Requests\SellerTransaction\DeleteRequest;
use App\Http\Requests\SellerTransaction\FetchRequest;
use App\Http\Requests\SellerTransaction\UpdateRequest;
use App\Http\Resources\SellerTransaction\SellerTransactionResource;
use App\Models\Role;
use App\Models\User;
use App\Services\SellerTransaction\SellerTransactionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class SellerTransactionController extends Controller
{
    private SellerTransactionService $sellerTransactionService;

    public function __construct ( SellerTransactionService $sellerTransactionService )
    {
        $this->sellerTransactionService = $sellerTransactionService;
    }

    public function list (): JsonResponse
    {
        $seller_transactions = $this->sellerTransactionService->list( Auth::user() );

        return response()->json( SellerTransactionResource::collection( $seller_transactions ) );
    }

    public function show ( FetchRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $seller_transaction = $this->sellerTransactionService->find( $validated_data[ 'id' ] );

        return response()->json( SellerTransactionResource::make( $seller_transaction ) );

    }

    public function update ( UpdateRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $seller_transaction              = $this->sellerTransactionService->find( $validated_data[ 'id' ] );
        $seller_transaction->shop_id     = $validated_data[ 'shop_id' ];
        $seller_transaction->refrence_id = $validated_data[ 'refrence_id' ];
        $seller_transaction->status      = $validated_data[ 'status' ];
        $seller_transaction->price       = $validated_data[ 'price' ];

        $seller_transaction = $this->sellerTransactionService->update( $seller_transaction );

        return response()->json( SellerTransactionResource::make( $seller_transaction ) );

    }

    public function delete ( DeleteRequest $request ): JsonResponse
    {
        $validated_data = $request->validated();

        $this->sellerTransactionService->delete( $validated_data[ 'id' ] );

        return response()->json( null, 204 );

    }
}
