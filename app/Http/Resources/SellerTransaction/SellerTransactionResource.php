<?php

namespace App\Http\Resources\SellerTransaction;

use App\Http\Resources\Order\OrderMetaResource;
use App\Http\Resources\Shop\ShopResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SellerTransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray ( $request )
    {
        return [
            'id'          => $this->id,
            'shop'        => ShopResource::make( $this->shop ),
            'order_metas' => OrderMetaResource::collection( $this->orderMetas ),
            'status'      => $this->status,
            'refrence_id' => $this->refrence_id,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
        ];
    }
}
