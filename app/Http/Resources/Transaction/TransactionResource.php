<?php

namespace App\Http\Resources\Transaction;

use App\Http\Resources\Order\OrderResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray ( $request )
    {
        return [
            'id'          => $this->id,
            'order'       => OrderResource::make( $this->order ),
            'price'       => $this->price,
            'refrence_id' => $this->refrence_id,
            'status'      => $this->status,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
        ];
    }
}
