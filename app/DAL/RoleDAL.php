<?php

namespace App\DAL;

use App\Models\Role;
use App\Models\User;

class RoleDAL extends BaseDAL
{

    protected function model (): string
    {
        return Role::class;
    }

    public function givePermissionToRole ( Role $role, string ...$permission ): void
    {
        $role->givePermissionTo( ...$permission );
    }

    public function assignRoleToUser ( User $user, string ...$role ): void
    {
        $user->assignRole( ...$role );
    }
}
