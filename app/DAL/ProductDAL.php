<?php

namespace App\DAL;

use App\Models\Product;

class ProductDAL extends BaseDAL
{

    protected function model (): string
    {
        return Product::class;
    }
}
