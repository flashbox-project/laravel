<?php

namespace App\DAL;

use App\Models\Permission;

class PermissionDAL extends BaseDAL
{

    protected function model (): string
    {
        return Permission::class;
    }
}
