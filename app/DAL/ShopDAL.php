<?php

namespace App\DAL;

use App\Models\Shop;

class ShopDAL extends BaseDAL
{

    protected function model (): string
    {
        return Shop::class;
    }

    public function nearbyList ( float $lat, float $long, float $radius )
    {
        return $this->model->select( 'shops.*' )->groupBy( 'id' )->havingRaw( '(6371 * acos( cos( radians( ? ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ? ) ) + sin( radians( ? ) ) * sin( radians( lat ) ) ) ) <= ?', [
            $lat,
            $long,
            $lat,
            $radius
        ] )->get();
    }
}
