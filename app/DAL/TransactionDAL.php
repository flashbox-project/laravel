<?php

namespace App\DAL;

use App\Models\Transaction;

class TransactionDAL extends BaseDAL
{

    protected function model (): string
    {
        return Transaction::class;
    }

    public function all ()
    {
        return $this->model->with( 'order' )->get();
    }

    public function allByUserID ( int $user_id ): array
    {
        return $this->model->with( [
            'order' => function ( $q1 ) use ( $user_id ) {
                $q1->where( 'orders.user_id', $user_id );
            }
        ] )->get();
    }
}
