<?php

namespace App\DAL;

use App\Models\User;

class UserDAL extends BaseDAL
{

    protected function model (): string
    {
        return User::class;
    }


}
