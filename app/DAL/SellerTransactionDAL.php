<?php

namespace App\DAL;

use App\Models\SellerTransaction;

class SellerTransactionDAL extends BaseDAL
{

    protected function model (): string
    {
        return SellerTransaction::class;
    }

    public function all ()
    {
        return $this->model->with( 'order' )->with( 'orderMetas' )->get();
    }

    public function allByUserID ( mixed $user_id ): array
    {
        $this->model->with( [
            'order' => function ( $q1 ) use ( $user_id ) {
                $q1->where( 'orders.user_id', $user_id );
            }
        ] )->with( 'orderMetas' )->get();
    }

}
