<?php

namespace App\Models;

use Spatie\Permission\Models\Permission as PermissionModel;

class Permission extends PermissionModel
{
    const PERMISSIONS = [
        // Category
        'list_category'             => 'category.list',
        'create_category'           => 'category.create',
        'show_category'             => 'category.show',
        'delete_category'           => 'category.delete',
        'update_category'           => 'category.update',

        // Shop
        'list_shop'                 => 'shop.list',
        'create_shop'               => 'shop.create',
        'show_shop'                 => 'shop.show',
        'update_shop'               => 'shop.update',
        'delete_shop'               => 'shop.delete',

        // Products
        'list_product'              => 'product.list',
        'create_product'            => 'product.create',
        'show_product'              => 'product.show',
        'update_product'            => 'product.update',
        'delete_product'            => 'product.delete',

        // Orders
        'list_order'                => 'order.list',
        'create_order'              => 'order.create',
        'show_order'                => 'order.show',
        'update_order'              => 'order.update',
        'delete_order'              => 'order.delete',

        // Order Metas
        'create_order_meta'         => 'order_meta.create',
        'delete_order_meta'         => 'order_meta.delete',

        // Transactions
        'list_transaction'          => 'transaction.list',
        'show_transaction'          => 'transaction.show',
        'update_transaction'        => 'transaction.update',
        'delete_transaction'        => 'transaction.delete',

        // Seller Transactions
        'list_seller_transaction'   => 'seller_transaction.list',
        'show_seller_transaction'   => 'seller_transaction.show',
        'update_seller_transaction' => 'seller_transaction.update',
        'delete_seller_transaction' => 'seller_transaction.delete',

        // Users
        'create_seller'             => 'seller.create',
        'list_user'                 => 'profile.list',
        'show_user'                 => 'user.show',
        'update_user'               => 'user.update',
        'delete_user'               => 'user.delete',

    ];
}
