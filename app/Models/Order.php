<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    const STATUS_CANCELED = 0;
    const STATUS_PAID = 1;
    const STATUS_OPEN = 2;

    protected $fillable = [
        'user_id',
        'status',
        'lat',
        'long',
    ];

    public function user (): BelongsTo
    {
        return $this->belongsTo( User::class );
    }

    public function metas (): HasMany
    {
        return $this->hasMany( OrderMeta::class );
    }
}
