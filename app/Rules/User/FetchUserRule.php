<?php

namespace App\Rules\User;

class FetchUserRule extends BaseUserRule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes ( $attribute, $value )
    {
        parent::passes( $attribute, $value );

        return true;
    }
}
