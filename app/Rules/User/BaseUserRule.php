<?php

namespace App\Rules\User;

use App\Models\Role;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class BaseUserRule implements Rule
{
    protected User $user;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct ()
    {
        $this->user = Auth::user();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes ( $attribute, $value )
    {
        if ( ! $this->user->hasRole( Role::ADMIN_ROLE_NAME ) && $value != $this->user->id )
        {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message ()
    {
        return 'Access Denied.';
    }
}
